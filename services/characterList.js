function CharacterList() {

	this.list = [];

	this.populateList = function(){
		this.list = [
		new Character('druid', 'balance', 'leather', 'intellect', 'vanquisher', 'damage'),
		new Character('druid', 'feral', 'leather', 'agility', 'vanquisher', 'damage'),
		new Character('druid', 'guardian', 'leather', 'agility', 'vanquisher', 'tank'),
		new Character('druid', 'restoration', 'leather', 'intellect', 'vanquisher', 'healer'),

		new Character('death knight', 'blood', 'plate', 'strength', 'vanquisher', 'tank'),
		new Character('death knight', 'frost', 'plate', 'strength', 'vanquisher', 'damage'),
		new Character('death knight', 'unholy', 'plate', 'strength', 'vanquisher', 'damage'),

		new Character('hunter', 'beast mastery', 'mail', 'agility', 'protector', 'damage'),
		new Character('hunter', 'marksmanship', 'mail', 'agility', 'protector', 'damage'),
		new Character('hunter', 'survival', 'mail', 'agility', 'protector', 'damage'),

		new Character('mage', 'arcane', 'cloth', 'intellect', 'vanquisher', 'damage'),
		new Character('mage', 'frost', 'cloth', 'intellect', 'vanquisher', 'damage'),
		new Character('mage', 'fire', 'cloth', 'intellect', 'vanquisher', 'damage'),

		new Character('monk', 'brewmaster', 'leather', 'agility', 'protector', 'tank'),
		new Character('monk', 'mistweaver', 'leather', 'agility', 'protector', 'healer'),
		new Character('monk', 'windwalker', 'leather', 'agility', 'protector', 'damage'),

		new Character('priest', 'discipline', 'cloth', 'intellect', 'conqueror', 'damage'),
		new Character('priest', 'holy', 'cloth', 'intellect', 'conqueror', 'healer'),
		new Character('priest', 'shadow', 'cloth', 'intellect', 'conqueror', 'healer'),

		new Character('paladin', 'holy', 'plate', 'intellect', 'conqueror', 'healer'),
		new Character('paladin', 'protection', 'plate', 'strength', 'conqueror', 'tank'),
		new Character('paladin', 'retribution', 'plate', 'strength', 'conqueror', 'damage'),

		new Character('rogue', 'combat', 'leather', 'agility', 'vanquisher', 'damage'),
		new Character('rogue', 'assassination', 'leather', 'agility', 'vanquisher', 'damage'),
		new Character('rogue', 'subtlety', 'leather', 'agility', 'vanquisher', 'damage'),

		new Character('shaman', 'elemental', 'mail', 'intellect', 'protector', 'damage'),
		new Character('shaman', 'enhancement', 'mail', 'agility', 'protector', 'damage'),
		new Character('shaman', 'restoration', 'mail', 'intellect', 'protector', 'healer'),

		new Character('warlock', 'affliction', 'cloth', 'intellect', 'conqueror', 'damage'),
		new Character('warlock', 'demonology', 'cloth', 'intellect', 'conqueror', 'damage'),
		new Character('warlock', 'destruction', 'cloth', 'intellect', 'conqueror', 'damage'),

		new Character('warrior', 'arms', 'plate', 'strength', 'protector', 'damage'),
		new Character('warrior', 'fury', 'plate', 'strength', 'protector', 'damage'),
		new Character('warrior', 'protection', 'plate', 'strength', 'protector', 'tank'),
		];
	}

	this.removeCharacter = function(character) {
		var list = this.list, i = this.list.indexOf(character);
		(i >= 0) ? this.list.splice(i, 1) : false;
	};

	this.addCharacter = function(newCharacter) {
		var list = this.list, conflicts = [];

		conflicts = this.list.filter(function(el){
			return el.conflictsWith(newCharacter);
		});

		if(conflicts.length === 0){
			list.push(newCharacter);
		}
	};

	this.removeConflicts = function(provided){
		if(provided.constructor != Array){ throw "removeConflicts requires an array." }

		for (var i = provided.length - 1; i >= 0; i--) {
			this.list = this.list.filter(function(el){
				return !el.conflictsWith(provided[i]);
			});
		}
	};

	this.formattedList = function(){
		return this.list.map(function(elem){
			return elem.full_display;
		}).join("\r");
	};

	}

