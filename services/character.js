Array.prototype.clone = function() {
	return this.slice(0);
};

String.prototype.capitalize = function(){
	return this.toLowerCase().replace( /\b\w/g, function (m) {
		return m.toUpperCase();
	});
};

function Character(klass, spec, armor_type, attribute, token_group, role) {
	this.klass = klass;
	this.spec = spec;
	this.armor_type = armor_type;
	this.attribute = attribute;
	this.token_group = token_group;
	this.role = role;
	this.display_name = spec.capitalize();
	this.klass_to_param = this.klass.replace(/ /g,"-");
	this.spec_to_param = this.spec.replace(/ /g,"-");
	this.row_css = this.klass_to_param + " " + this.spec_to_param ;
	this.spec_css = this.klass_to_param + "-" + this.spec_to_param;
	this.full_display = klass.capitalize() + " ("
		+ spec.capitalize() +  ")";


	this.conflictsWith = function (newCharacter) {
		var conflicts = [];
		if (this.armor_type == newCharacter.armor_type){
			conflicts.push("Disqualified by armory type.");
		}

		if(this.attribute == newCharacter.attribute){
			conflicts.push("Disqualified by attribute.");
		}

		if(this.token_group == newCharacter.token_group) {
			conflicts.push("Disqualified by token group.");
		}

		if(newCharacter.role == 'tank' && this.role == newCharacter.role) {
			conflicts.push("Disqualified by role.");
		}

		if(conflicts.length > 0){
			return true;
		} else {
			return false;
		}
	};
}
