var app = angular.module('plunker', []);

app.controller('MainCtrl', function($scope) {
	console.log($scope);
	var vm = this;
	vm.availableCharacters = new CharacterList();
	vm.attendingCharacters = new CharacterList();
	vm.availableCharacters.populateList();
});

app.directive('availableCharacters', function(){
	return {
		restrict: 'E',
		templateUrl: './partials/_characters.html',
		scope: true,
		controllerAs: 'vm',
		controller: function($scope){
			this.characters = $scope.main.availableCharacters;
			attending = $scope.main.attendingCharacters;
			this.swapList = function(character){
					this.characters.removeConflicts([character]);
					attending.addCharacter(character);
			};
		},
	};
});

app.directive('attendingCharacters', function(){
	return {
		restrict: 'E',
		templateUrl: './partials/_characters.html',
		scope: true,
		controllerAs: 'vm',
		controller: function($scope){
			this.characters = $scope.main.attendingCharacters;
			available = $scope.main.availableCharacters;
			this.swapList = function(character){
					this.characters.removeCharacter(character);
					available.populateList();
					available.removeConflicts(this.characters.list);
			};
		},
	};
});

app.directive('copybox', function(){
	return {
		restrict: 'E',
		templateUrl: './partials/_copybox.html',
		scope: true,
	};
});
